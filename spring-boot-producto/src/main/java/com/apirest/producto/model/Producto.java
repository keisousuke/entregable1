package com.apirest.producto.model;

import com.apirest.producto.model.common.Generic;

import java.util.ArrayList;
import java.util.List;

public class Producto extends Generic {
    public String descripcion;
    public double precio;
    public final List<Proveedor> idsProveedores = new ArrayList<>();

    public Producto() {
    }

    public Producto(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }
}
