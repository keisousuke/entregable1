package com.apirest.producto.service.impl;

import com.apirest.producto.model.Producto;
import com.apirest.producto.service.ProductService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Component
public class ProductServiceImpl implements ProductService {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Producto> productos = new ConcurrentHashMap<Long,Producto>();

    public ProductServiceImpl() {
        long i = this.secuenciador.incrementAndGet();
        this.productos.put(i, new Producto(i, "Laptop Samsung", 300.50));
        i = this.secuenciador.incrementAndGet();
        this.productos.put(i, new Producto(i, "Televisor Sony", 350.00));
        i = this.secuenciador.incrementAndGet();
        this.productos.put(i, new Producto(i, "Playstation", 300.00));
        i = this.secuenciador.incrementAndGet();
        this.productos.put(i, new Producto(i, "Xbox", 250.75));
        i = this.secuenciador.incrementAndGet();
        this.productos.put(i, new Producto(i, "Nintento", 220.00));
    }

    @Override
    public List<Producto> getProductos() {
        return this.productos
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    @Override
    public Producto getProducto(long index) throws IndexOutOfBoundsException {
        final Producto p = this.productos.get(index);
        return p;
    }

    @Override
    public Producto addProducto(Producto p) {
        p.id = this.secuenciador.incrementAndGet();
        this.productos.put(p.id, p);

        return p;
    }

    @Override
    public Producto updateProducto(long index, Producto p) throws IndexOutOfBoundsException {
        p.id = index;
        this.productos.put(index, p);
        return p;
    }

    @Override
    public void removeProducto(long index) throws IndexOutOfBoundsException {
        this.productos.remove(index);
    }

    @Override
    public int getIndex(long index) throws IndexOutOfBoundsException {
        return 0;
    }
}
