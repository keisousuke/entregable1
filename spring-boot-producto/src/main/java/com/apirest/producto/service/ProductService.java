package com.apirest.producto.service;

import com.apirest.producto.model.Producto;

import java.util.List;

public interface ProductService {
    List<Producto> getProductos();
    Producto getProducto(long index) throws IndexOutOfBoundsException;
    Producto addProducto(Producto newPro);
    Producto updateProducto(long index, Producto newPro)throws IndexOutOfBoundsException;
    void removeProducto(long index) throws IndexOutOfBoundsException;
    int getIndex(long index) throws IndexOutOfBoundsException;
}
